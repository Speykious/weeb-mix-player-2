import { ArgTypeTuple } from 'commands-ts'
import { GuildMember, TextChannel } from 'discord.js'
import {
	Parser,
	choice,
	sequenceOf,
	tuple,
	many,
	spaces,
	ParserState
} from 'parsers-ts'
import { WMPCommand, WMPCommandInfo, WMPCommandResult } from './WMPCommand'
import { WeebMusicPlayer } from './WeebMusicPlayer'

export class WMPCommandManager {
	public commands: WMPCommand[]
	public types: ArgTypeTuple<any[]>
	public parser: (
		author: GuildMember,
		channel: TextChannel,
		bot: WeebMusicPlayer
	) => Parser<WMPCommandResult>

	constructor(types: ArgTypeTuple<any[]>, commandinfos: WMPCommandInfo[]) {
		this.commands = commandinfos.map((commandinfo) => new WMPCommand(types, commandinfo))
		this.types = types
		
		this.parser = (author, channel, bot) =>
			choice(...this.commands.map(command => command.nameParser))
				.mapError(`Command not found`)
				.chain((command: WMPCommand) => {
					if (author.id !== bot.owner.id) {
						if (
							command.userPermissions &&
							!author.permissions.has(command.userPermissions)
						)
							return new Parser((inputState) =>
								inputState.errorify(
									`⛔ **Nope** ⛔ - You don't have the permissions required.`
								)
							)
						if (
							command.botPermissions &&
							!author.guild.members.cache
								.get(bot.user.id)
								.permissions.has(command.botPermissions)
						)
							return new Parser((inputState) =>
								inputState.errorify(
									`⛔ **Nope** ⛔ - I don't have the permissions required.`
								)
							)
					}

					return sequenceOf(
						tuple(many(spaces), command.parser)
					).map((results) => ({ ...results[1], author, channel, bot }))
				})
	}

	/** CommandManager parser function. */
	parse(
		author: GuildMember,
		channel: TextChannel,
		bot: WeebMusicPlayer,
		targetString: string,
		index: number = 0
	) {
		const finalState = this.parser(author, channel, bot)
			.transformer(new ParserState({ targetString, index }))
		
		if (!finalState.error && finalState.index < targetString.length)
			finalState.error = {
				info: `Unknown arguments at index ${finalState.index}: \`${
					targetString.slice(finalState.index)
				}\``,
				index: finalState.index,
				unknownArgs: targetString.slice(finalState.index)
			}
		
		return finalState
	}

	/** Parses a string and executes the command. If the command isn't valid, it calls the errorCallback. */
	runSync(
		author: GuildMember,
		channel: TextChannel,
		bot: WeebMusicPlayer,
		targetString: string,
		errorCallback: (state: ParserState<WMPCommandResult>) => any
	) {
		const state = this.parse(author, channel, bot, targetString)
		if (state.error) return errorCallback(state)
		state.result.execute(state.result)
	}

	/** Parses a string and executes the command. If the command isn't valid, it Promise.reject's the state. */
	async run(
		author: GuildMember,
		channel: TextChannel,
		bot: WeebMusicPlayer,
		targetString: string
	) {
		const state = this.parse(author, channel, bot, targetString)
		if (state.error) return Promise.reject(state)
		state.result.execute(state.result)
		return Promise.resolve(state)
	}
}
