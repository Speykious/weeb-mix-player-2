import * as xlsx from 'xlsx'
import { rpath } from './index'
import { Timestamp } from './Timestamp'
import { writeFileSync } from 'fs'

interface Cell {
	t: 'n' | 's'
	v: any
	f?: string
	w: string
}

export interface Song {
	OP_ED: 'OP' | 'ED' | 'OST'
	n: number
	Animé: string
	Begins: Timestamp
	Title: string
	Artist: string
	Duration: Timestamp
	URL: string
	index: number
}

export class Playlist {
	public songs: Song[]

	constructor(odspath: string) {
		const songs: Song[] = []

		const jsonData = xlsx.readFile(rpath(odspath))
		const cells: { [key: string]: Cell } =
			jsonData.Sheets[jsonData.SheetNames[0]]
		for (const [name, cell] of new Map(Object.entries(cells))) {
			if (/^[A-Z]1(0[3-9])?$/.test(name)) continue
			const ci = Number(name.slice(1)) - 3

			switch (name[0]) {
				case 'A':
					songs[ci] = { index: ci } as Song
					break
				case 'B':
					let s: 'OP' | 'ED' | 'OST'
					switch (cell.w) {
						case 'Opening':
							s = 'OP'
							break
						case 'Ending':
							s = 'ED'
							break
						default:
							s = 'OST'
							break
					}
					songs[ci].OP_ED = s
					break
				case 'C':
					songs[ci].n = cell.v
					break
				case 'D':
					songs[ci].Animé = cell.w
					break
				case 'E':
					songs[ci].Begins = new Timestamp(cell.w)
					break
				case 'F':
					songs[ci].Title = cell.w
					break
				case 'G':
					songs[ci].Artist = cell.w
					break
				case 'H':
					if (cell.w !== '00:00:00' && cell.w[0] !== '-')
						songs[ci].Duration = new Timestamp(cell.w)
					// else delete songs[ci]
					break
				case 'I':
					songs[ci].URL = cell.w
					break
			}
		}

		this.songs = songs
	}

	present(index: number) {
		const s = this.songs[index]
		return `\`${(index + '').padStart(2, '0')}\`・\`${s.Begins.ccc}\` - **[${
			s.OP_ED
		} ${s.n}]** ${s.Animé} - *__${s.Title}__*, ${s.Artist} **[${
			s.Duration.cc
		}]**`
	}

	mCurrentTitle(index: number, surround: number = 3) {
		const titles = []
		for (let i = index - surround; i <= index + surround; i++) {
			if (i >= 0 && i < this.songs.length) titles.push(this.present(i))
		}
		return titles.join('\n')
	}

	get secIntervals() {
		return this.songs.map((song) => song.Begins.totalSeconds)
	}

	getSongIndexAt(seconds: number) {
		const markers = this.secIntervals
		
		for (const si in markers) {
			const i = Number(si)
			if (markers[i] > seconds) {
				if (i < 1) return 0
				else return i - 1
			}
		}

		return this.songs.length - 1
	}

	getSongAt(seconds: number) {
		return this.songs[this.getSongIndexAt(seconds)]
	}
}
