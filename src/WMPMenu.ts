import { Message, MessageEmbed } from 'discord.js'
import { Song } from './Playlist'

/** Adds information about a song as fields in an embed.
 * @param embed The embed to add fields to.
 * @param song The song to get information from.
 * @returns The embed to which we have added fields. */
const addSongFields = (embed: MessageEmbed, song: Song) => {
	embed
		.addField(
			`[${(song.index + 1).toString().padStart(3, '0')
			}] - Animé ${song.OP_ED} n°${song.n} from`,
			`**━━━━　${song.Animé}　━━━━**`
		)
		.addField('Artist', song.Artist, true)
		.addField('Title', `*__${song.Title}__*`, true)
		.addField('Duration', song.Duration.cc, true)
		.setImage(song.URL)
	return embed
}

export interface WMPMenuJSON {
	menus: {
		channel: string
		message: string
	}[]
}

/** Information used to create a WMPMenu object. */
export interface WMPMenuInfo {
	/** The message where the menu is displayed. */
	message: Message
	/** The picture shown in the menu when in stop mode. */
	stopThumbnail: string
	/** The picture shown in the menu when in pause mode. */
	pauseThumbnail: string
	/** The footer of the embed in the message of the menu. */
	footer: string
}

/** A class that represents a Menu for the Weeb Mix Player. */
export class WMPMenu {
	/** The message where the menu is displayed. */
	public message: Message
	/** The picture shown in the menu when in stop mode. */
	public stopThumbnail: string
	/** The picture shown in the menu when in pause mode. */
	public pauseThumbnail: string
	/** The footer of the embed in the message of the menu. */
	public footer: string

	/** Whether it displays song data or playlist data. */
	public displayMode: 'song' | 'playlist'
	/** The playing mode. */
	public playMode: 'stop' | 'play' | 'pause'
	/** The current page index of the playlist display. */
	public currentPageIndex: number

	/** Creates a WMPMenu object.
	 * @param info The info needed to create the object.*/
	constructor(info: WMPMenuInfo) {
		this.message = info.message
		this.stopThumbnail = info.stopThumbnail
		this.pauseThumbnail = info.pauseThumbnail
		this.footer = info.footer

		this.currentPageIndex = 0
		this.displayMode = 'song'
		this.editStop()
	}

	/**
	 * Switches between displaying a playlist page and a song.
	 * @param songs The songs of the playlist.
	 * @param mixTitle The title of the mix playing.
	 * @param currentSong The current song playing.
	 */
	switchMode(songs: Song[], mixTitle: string, currentSong: Song) {
		switch (this.displayMode) {
			case 'song':
				this.displayMode = 'playlist'

				return this.displayPage(songs)
			case 'playlist':
				this.displayMode = 'song'

				switch (this.playMode) {
					case 'stop':  return this.editStop()
					case 'play':  return this.editPlay(mixTitle, currentSong)
					case 'pause': return this.editPause(mixTitle, currentSong)
				}
		}
	}

	/** Edits the menu message to stop mode.
	 * @returns A Promise of the edited message. */
	editStop() {
		this.playMode = 'stop'
		if (this.displayMode !== 'song')
			return Promise.resolve(this.message)
		
		return this.message.edit(
			new MessageEmbed()
				.setFooter(this.footer)
				.setColor(0x323264)
				.setTitle(`Nothing is playing right now :(`)
				.setImage(this.stopThumbnail)
		)
	}

	/** Edits the menu message to play mode.
	 * @param mixTitle The title of the mix playing.
	 * @param currentSong The current song playing.
	 * @returns A Promise of the edited message. */
	editPlay(mixTitle: string, currentSong: Song) {
		this.playMode = 'play'
		if (this.displayMode !== 'song')
			return Promise.resolve(this.message)
		
		return this.message.edit(
			addSongFields(
				new MessageEmbed()
					.setFooter(this.footer)
					.setColor(0x64ff64)
					.setTitle(`▶ [Playing] ${mixTitle}`),
				currentSong
			)
		).catch(console.trace)
	}

	/** Edits the menu message to pause mode.
	 * @param mixTitle The title of the mix playing.
	 * @param currentSong The current song playing.
	 * @returns A Promise of the edited message. */
	editPause(mixTitle: string, currentSong: Song) {
		this.playMode = 'pause'
		if (this.displayMode !== 'song')
			return Promise.resolve(this.message)
		
		return this.message.edit(
			addSongFields(
				new MessageEmbed()
					.setFooter(this.footer)
					.setColor(0xc0c032)
					.setTitle(`⏹ [Paused] ${mixTitle}`),
				currentSong
			).setImage(this.pauseThumbnail)
		).catch(console.trace)
	}

	/** Displays the current page of the playlist.
	 * @param songs The songs of the playlist. */
	displayPage(songs: Song[]) {
		const cpi = this.currentPageIndex
		const slice = songs.slice(cpi * 20, (cpi + 1) * 20)

		if (this.displayMode !== 'playlist')
			return Promise.resolve(this.message)
		
		return this.message.edit(
			new MessageEmbed()
				.setTitle('Playlist Embed')
				.setColor(0xffff32)
				.addField('Idx', slice.map(s => (s.index + 1).toString().padStart(3, '0')).join('\n'), true)
				.addField('Animé',             slice.map(s => s.Animé).join('\n'), true)
				.addField('Title of the song', slice.map(s => s.Title).join('\n'), true)
				.setFooter(`Page n°${cpi + 1} - ${this.footer}`)
		)
	}

	/** Displays the next page of the playlist.
	 * @param songs The songs of the playlist. */
	gotoNextPage(songs: Song[]) {
		this.currentPageIndex++
		if (this.currentPageIndex === Math.ceil(songs.length / 20))
			this.currentPageIndex = 0
		
		this.displayPage(songs)
	}

	/** Displays the previous page of the playlist.
	 * @param songs The songs of the playlist. */
	gotoPrevPage(songs: Song[]) {
		this.currentPageIndex--
		if (this.currentPageIndex === -1)
			this.currentPageIndex = Math.ceil(songs.length / 20) - 1
		
		this.displayPage(songs)
	}
}
