import { CommandInfo, Command, ArgTypeTuple, CommandResult } from 'commands-ts'
import { GuildMember, TextChannel } from 'discord.js'
import { WeebMusicPlayer } from './WeebMusicPlayer'

export interface WMPCommandResult extends CommandResult {
	author: GuildMember
	channel: TextChannel
	bot: WeebMusicPlayer
}

export interface WMPCommandInfo extends CommandInfo {
	execute: (input: WMPCommandResult) => void
	userPermissions?: number
	botPermissions?: number
}

export class WMPCommand extends Command {
	public userPermissions?: number
	public botPermissions?: number

	constructor(types: ArgTypeTuple<any[]>, info: WMPCommandInfo) {
		super(types, info)

		if (info.userPermissions) this.userPermissions = info.userPermissions
		if (info.botPermissions) this.botPermissions = info.botPermissions
	}
}
