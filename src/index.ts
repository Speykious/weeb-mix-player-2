import * as path from 'path'
import { TextChannel, DMChannel, NewsChannel, Message, User } from 'discord.js'
import { WeebMusicPlayer } from './WeebMusicPlayer'
import { str, choice, sequenceOf, tuple, uint } from 'parsers-ts'
import { Timestamp } from './Timestamp'
import { cmdi_play } from './commands/play'
import { cmdi_menu } from './commands/menu'

export const rpath = (s: string) => path.resolve(__dirname, s)
export const delay = async (ms: number) =>
	new Promise((resolve) => setTimeout(resolve, ms))

export const sendError = async (
	channel: TextChannel | DMChannel | NewsChannel,
	errorString: string,
	msDelay: number = 5000
) => {
	const errorMsg = await channel
		.send(errorString)
		.catch(() => Promise.resolve())
	if (errorMsg) errorMsg.delete({ timeout: msDelay })
	return Promise.resolve(errorMsg)
}

import * as dotenvFlow from 'dotenv-flow'
dotenvFlow.config()

const colon = str(':')

const bot = new WeebMusicPlayer({
	prefix: 'plz:',
	ownerId: '358960666238910465',
	readyString: '🎵 Ready for some REAL weeb music 😎 🎵',
	footer: 'Weeb Music Bot 2.1 - by Speykious',
	mixName: '**__ΩMEGAGIGATERAMIX__** - by Speykious',
	stopThumbnail: 'https://i.imgur.com/ca1XH7p.jpg',
	pauseThumbnail: 'https://i.imgur.com/z2zcY7y.jpg',
	mixFile: '../megagigateramix.mp3',
	playlistFile: '../playlist-table.ods',
	menusFile: '../menus.json'
})
	.registerDefaultTypes()
	.registerTypes({
		name: 'timestamp',
		description: 'A timestamp.',
		label: 'timestamp',
		parser: choice(
			sequenceOf(tuple(uint, colon, uint, colon, uint)),
			sequenceOf(tuple(uint, str('h'), uint, str('m'), uint, str('s')))
		).map((result) => new Timestamp(`${result[0]}:${result[2]}:${result[4]}`))
	})
	.registerCommands(cmdi_play, cmdi_menu)

bot.on('message', async (msg: Message) => {
	if (msg.author.id === bot.user.id) return // console.error(`You are ME.`)

	if (msg.channel.type !== 'text') return // console.error(`No text channel.`)
	if (!msg.content.startsWith(bot.prefix)) return // console.error(`No prefix.`)
	msg.delete({ timeout: 1 })

	const cmd = msg.content.slice(bot.prefix.length)
	bot.execute(cmd, msg)
})

bot.on('messageReactionAdd', async (reaction, user: User) => {
	if (user.bot) return

	const menu = bot.getMenuFromMessage(reaction.message.id)
	if (!menu) return
	const current = menu === bot.currentMenu
	reaction.users.remove(user)

	const member = await menu.message.guild.members.fetch({ user })
	const channel = menu.message.channel as TextChannel

	switch (reaction.emoji.name) {
		case '⏯':
			if (!current) return
			return bot.playpause()
		case '⏹':
			if (!current) return
			return bot.stop()
		case '⏮':
			if (!current) return
			return bot.gotoPrevTrack(member, channel)
		case '🔁':
			if (!current) return
			return bot.replayTrack(member, channel)
		case '⏭':
			if (!current) return
			return bot.gotoNextTrack(member, channel)
		
		case '⬅️': return bot.gotoPrevPage(menu)
		case '🎵': return bot.switchMode(menu)
		case '➡️': return bot.gotoNextPage(menu)
		case '🚫': return bot.deleteMenu(member, menu)
	}
})

bot.login(process.env.WMP_LOCAL_TOKEN)
