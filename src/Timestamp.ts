export interface TimestampInfo {
	hours?: number
	minutes?: number
	seconds?: number
}

export class Timestamp {
	public hours: number
	public minutes: number
	public seconds: number

	constructor(t?: string | TimestampInfo) {
		let hours: number, minutes: number, seconds: number
		if (typeof t === 'string') {
			if (!/^(\d+):(\d+):(\d+)$/.test(t))
				throw new Error('wrong format for timestamp!')

			const fs = t.indexOf(':'),
				ls = t.lastIndexOf(':')
			hours = Number(t.slice(0, fs))
			minutes = Number(t.slice(fs + 1, ls))
			seconds = Number(t.slice(ls + 1))
		} else {
			if (!t) t = {}
			if (t.hours) hours = t.hours
			else hours = 0
			if (t.minutes) minutes = t.minutes
			else minutes = 0
			if (t.seconds) seconds = t.seconds
			else seconds = 0
		}

		this.hours = hours + Math.floor(minutes / 60)
		this.minutes = (minutes % 60) + Math.floor(seconds / 60)
		this.seconds = seconds % 60
	}

	get strHours() {
		return this.hours.toString().padStart(2, '0')
	}

	get strMinutes() {
		return this.minutes.toString().padStart(2, '0')
	}

	get strSeconds() {
		return this.seconds.toString().padStart(2, '0')
	}

	get totalSeconds() {
		return this.hours * 3600 + this.minutes * 60 + this.seconds
	}

	get hms() {
		return `${this.strHours}h${this.strMinutes}m${this.strSeconds}s`
	}

	get ms() {
		return `${this.strMinutes}m${this.strSeconds}s`
	}

	get ccc() {
		return `${this.strHours}:${this.strMinutes}:${this.strSeconds}`
	}

	get cc() {
		return `${this.strMinutes}:${this.strSeconds}`
	}
}
