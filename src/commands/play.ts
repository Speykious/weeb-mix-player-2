import { WMPCommandInfo } from '../WMPCommand'
import { sendError } from '..'
import { Timestamp } from '../Timestamp'
import * as FuzzySearch from 'fuzzy-search'

export const countRepeats = <T>(list: T[]) => {
	const scores: [T, number][] = []
	for (const element of list) {
		const existing = scores.findIndex(score => score[0] === element)
		if (existing === -1)
			scores.push([element, 1])
		else
			scores[existing][1]++
	}
	return scores.sort(score => score[1])
}

export const cmdi_play: WMPCommandInfo = {
	name: 'play',
	description: 'Plays the mix.',
	options: [
		{
			name: 'index',
			description: 'Go to the song at some index.',
			short: 'i',
			arguments: [
				{
					name: 'i',
					description: 'The index of the song.',
					type: 'int',
					min: 1,
					max: 100
				}
			]
		},
		{
			name: 'title',
			description: 'Search for a particular title.',
			short: 't',
			arguments: [
				{
					name: 'search',
					description: 'The text used to search a song title.',
					type: 'text'
				}
			]
		},
		{
			name: 'seek',
			description: 'Go to the song at some timestamp.',
			short: 's',
			arguments: [
				{
					name: 'timestamp',
					description: 'The timestamp.',
					type: 'timestamp'
				}
			]
		}
	],

	execute: input => {
		const { bot, author, channel, opts } = input
		const menu = bot.getMenuFromChannel(input.channel.id)
		if (!menu) {
			sendError(input.channel, `馬鹿, There is no menu in this channel.`)
			return
		}

		switch (opts.length) {
			case 0:
				bot.play(author, channel, menu)
				break
			case 1:
				const option = opts[0]
				switch (option.name) {
					case 'index':
						const index = option.argsResults[0].value as number
						bot.play(author, channel, menu, bot.playlist.songs[index-1].Begins.totalSeconds)
						break
					case 'title':
						const searcher = new FuzzySearch(bot.playlist.songs, [
							"Animé", "Artist", "OP_ED", "Title"
						], { caseSensitive: false, sort: true })

						const searchings = countRepeats(
							(option.argsResults[0].value as string).split(/\s+/)
							.map(searching => searcher.search(searching)).flat()
						)

						if (searchings.length === 0)
							return sendError(input.channel, `Could not find any title :(`)

						bot.play(author, channel, menu, searchings[0][0].Begins.totalSeconds)
						break
					case 'seek':
						const timestamp = option.argsResults[0].value as Timestamp
						bot.play(author, channel, menu, timestamp.totalSeconds as number)
						break
				}
				break
			default:
				sendError(input.channel, `You cannot have more than one option.`)
				break
		}
	}
}
