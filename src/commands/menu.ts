import { WMPCommandInfo } from '../WMPCommand'
import { Permissions } from 'discord.js'

export const cmdi_menu: WMPCommandInfo = {
	name: 'menu',
	description: 'Creates a menu.',
	userPermissions: Permissions.FLAGS.ADMINISTRATOR,
	execute: input => input.bot.createMenu(input.channel)
}
