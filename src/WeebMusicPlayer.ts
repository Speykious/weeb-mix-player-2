import {
	Client,
	ClientOptions,
	User,
	TextChannel,
	VoiceChannel,
	StreamDispatcher,
	MessageEmbed,
	Message,
	GuildMember,
	Permissions
} from 'discord.js'

import { WMPMenu, WMPMenuJSON } from './WMPMenu'
import { readFileSync, writeFileSync } from 'fs'
import { rpath, sendError, delay } from '.'
import { Song, Playlist } from './Playlist'
import { WMPCommandInfo } from './WMPCommand'
import { WMPCommandManager } from './WMPCommandManager'
import { defaultTypes, ArgTypeTuple, ArgType } from 'commands-ts'

export interface WMPOptions {
	/** The command prefix of the Weeb Mix Player. */
	prefix: string
	/** The ID of the owner of the bot. */
	ownerId: string
	/** The picture shown in the menu when in stop mode. */
	stopThumbnail: string
	/** The picture shown in the menu when in pause mode. */
	pauseThumbnail: string
	/** The footer of the embed in the message of the menu. */
	footer: string
	/** The path to the .mp3 mix file. */
	mixFile: string
	/** The name of the mix. Will be taken from the mixFile path by default. */
	mixName?: string
	/** The path to the .ods playlist file. */
	playlistFile: string
	/** The string to log in the console when ready. */
	readyString?: string
	/** The path to the file where the menus data is saved. */
	menusFile?: string
}

/** A discord bot that plays a giant mix of weeb songs. */
export class WeebMusicPlayer extends Client {
	/** The command prefix of the Weeb Mix Player. */
	public prefix: string
	/** The owner of the bot. */
	public owner: User

	/** The different types of the arguments of the commands. */
	public types: ArgTypeTuple<any[]>
	/** The command manager of the bot. */
	public commandManager: WMPCommandManager

	/** The list of all the menus. */
	public menus: WMPMenu[]
	/** The path to the file where the menus data is saved. */
	public menusFile: string

	/** The picture shown in the menu when in stop mode. */
	public stopThumbnail: string
	/** The picture shown in the menu when in pause mode. */
	public pauseThumbnail: string
	/** The footer of the embed in the message of the menu. */
	public footer: string

	/** The playlist containing all the songs data. */
	public playlist: Playlist
	/** The path to the .mp3 mix file. */
	public mixFile: string
	/** The name of the mix that is being played. */
	public mixName: string
	/** The last seconds seeked. */
	public lastSeeked: number
	/** The current song playing. */
	public currentSong: Song
	/** The current menu playing. */
	public currentMenu: WMPMenu

	/** The voice channel where the bot is playing the mix. */
	public voiceChannel: VoiceChannel
	/** The player that plays the mix. */
	public player: StreamDispatcher

	/** Creates a WeebMusicPlayer object. */
	constructor(options: WMPOptions, superOptions?: ClientOptions) {
		super(superOptions)
		this.prefix = options.prefix
		this.types = new ArgTypeTuple()

		this.mixFile = rpath(options.mixFile)
		if (options.mixName) this.mixName = options.mixName
		else
			this.mixName = this.mixFile.substring(this.mixFile.lastIndexOf('/') + 1)

		this.menus = []
		this.playlist = new Playlist(options.playlistFile)

		if (options.stopThumbnail) this.stopThumbnail = options.stopThumbnail
		if (options.pauseThumbnail) this.pauseThumbnail = options.pauseThumbnail
		if (options.footer) this.footer = options.footer

		this.once('ready', async () => {
			try {
				this.owner = await this.users.fetch(options.ownerId, true)
				if (!this.owner)
					throw new Error(`Owner "${options.ownerId}" hasn't been found`)
			} catch (err) {
				console.error(err)
				return this.destroy()
			}

			if (options.menusFile) {
				this.menusFile = rpath(options.menusFile)
				const raw = readFileSync(this.menusFile, { encoding: 'utf8' })
				const { menus } = JSON.parse(raw) as WMPMenuJSON

				if (menus) {
					if (!(menus instanceof Array))
						throw new Error(`The menus child is not an array`)

					let counter = 0
					for (const menu of menus) {
						try {
							// Make sure that the menu is a menu
							if (!menu.channel)
								throw new Error(`Menu n°${counter} doesn't have a channel`)
							if (!menu.message)
								throw new Error(`Menu n°${counter} doesn't have a message`)

							// Make sure that the channel exists
							const channel = await this.channels.fetch(menu.channel, true)
							if (!channel) throw new Error(`Channel doesn't exist`)
							if (!(channel instanceof TextChannel))
								throw new Error(
									`Channel "${menu.channel}" from Menu n°${counter} is not a TextChannel`
								)

							// Make sure that the message exists within the channel
							const message = await channel.messages.fetch(menu.message, true)
							if (!message)
								throw new Error(
									`Message "${menu.message}" from Menu n°${counter} doesn't exist`
								)

							// Push the constructed menu to the list of menus
							this.stopThumbnail = options.stopThumbnail
							this.pauseThumbnail = options.pauseThumbnail
							this.footer = options.footer
							this.menus.push(
								new WMPMenu({
									message,
									stopThumbnail: this.stopThumbnail,
									pauseThumbnail: this.pauseThumbnail,
									footer: this.footer
								})
							)
						} catch (err) {
							// Display the error: if there is one, the concerned
							// menu will just be ignored and the loop will continue
							console.error(err)
						}

						counter++
					}
				}
			} else {
				this.menusFile = rpath(`../menus.json`)
				writeFileSync(this.menusFile, '{}', { encoding: 'utf8' })
			}

			if (options.readyString) console.log(options.readyString)
		})

		this.once('disconnect', () => console.log('Disconnected.'))
		this.lastSeeked = 0
	}

	// Creates a menu.
	async createMenu(channel: TextChannel) {
		try {
			const menuMsg = await channel.send(
				new MessageEmbed().setTitle(`Creating the menu...`)
			)
			
			for (const emoji of ['⏯', '⏹', '⏮', '🔁', '⏭', '⬅️', '🎵', '➡️'])
				await menuMsg.react(emoji)
			
			const menu = new WMPMenu({
				message: menuMsg,
				stopThumbnail: this.stopThumbnail,
				pauseThumbnail: this.pauseThumbnail,
				footer: this.footer
			})

			this.menus.push(menu)
			this.saveMenus()

			return Promise.resolve(menu)
		} catch (err) {
			await sendError(channel, `**ERROR:** ${err}`)
			return Promise.resolve()
		}
	}

	deleteMenu(member: GuildMember, menu: WMPMenu) {
		if (!member.hasPermission(Permissions.FLAGS.ADMINISTRATOR)
		&& member.id !== this.owner.id) return

		if (!this.menus.some(m => m === menu)) return

		menu.message.delete()
		this.menus = this.menus.filter(m => m === menu)
		this.saveMenus()
	}

	saveMenus() {
		writeFileSync(this.menusFile, JSON.stringify({
			menus: this.menus.map(menu => ({
				channel: menu.message.channel.id,
				message: menu.message.id
			}))
		}, null, '\t'), { encoding: 'utf8' })
	}

	// Gets a menu from a specific channel
	getMenuFromChannel(channelId: string) {
		const lmenu = this.menus.filter(
			(menu) => menu.message.channel.id === channelId
		)
		if (lmenu && lmenu.length > 0) return lmenu[0]
		else return null
	}

	// Gets a menu from a specific message
	getMenuFromMessage(messageId: string) {
		const lmenu = this.menus.filter((menu) => menu.message.id === messageId)
		if (lmenu && lmenu.length > 0) return lmenu[0]
		else return null
	}

	// Plays the mix in a voice channel.
	async play(member: GuildMember, channel: TextChannel, menu: WMPMenu, seek: number = 0) {
		try {
			this.editStop()
			const voiceChannel = member.voice.channel
			// tslint:disable-next-line: no-string-throw
			if (!voiceChannel) throw `馬鹿, ur not in a voice channel`

			const connection = await voiceChannel.join()
			this.voiceChannel = voiceChannel
			this.player = connection
				.play(this.mixFile, { volume: false, seek })
				.on('error', (err) =>
					sendError(channel, `**ERROR while playing:** ${err}`)
				)
				.on('finish', () => this.stop())

			this.currentMenu = menu
			this.lastSeeked = seek
			this.currentSong = this.playlist.getSongAt(seek)
			this.editPlay()

			await this.follow()

			return Promise.resolve(this.player)
		} catch (err) {
			await sendError(channel, `**ERROR:** ${err}`)
			this.stop()
			return Promise.resolve()
		}
	}

	editPlay() {
		if (this.currentMenu)
			this.currentMenu.editPlay(this.mixName, this.currentSong)

		return this
	}

	editPause() {
		if (this.currentMenu)
			this.currentMenu.editPause(this.mixName, this.currentSong)

		return this
	}

	editStop() {
		if (this.currentMenu)
			this.currentMenu.editStop()

		return this
	}

	switchMode(menu: WMPMenu) {
		menu.switchMode(this.playlist.songs, this.mixName, this.currentSong)

		return this
	}

	displayPage(menu: WMPMenu) {
		menu.displayPage(this.playlist.songs)
		
		return this
	}

	gotoPrevPage(menu: WMPMenu) {
		menu.gotoPrevPage(this.playlist.songs)

		return this
	}

	gotoNextPage(menu: WMPMenu) {
		menu.gotoNextPage(this.playlist.songs)
		
		return this
	}

	get elapsedSeconds() {
		return this.lastSeeked + Math.floor(this.player.streamTime / 1000)
	}

	/** Follows the elapsed seconds to update the current menu accordingly. */
	async follow() {
		if (!this.voiceChannel) {
			return Promise.resolve()
		}

		const currentSong = this.playlist.getSongAt(this.elapsedSeconds)
		if (currentSong !== this.currentSong) {
			this.currentSong = currentSong
			this.editPlay()
		}

		await delay(1000)
		this.follow()

		return Promise.resolve()
	}

	/** Goes to the next track. */
	gotoNextTrack(member: GuildMember, channel: TextChannel) {
		const currentSongIndex = this.playlist.getSongIndexAt(this.elapsedSeconds)
		if (currentSongIndex >= this.playlist.songs.length - 1) return this.stop()
		this.play(
			member, channel, this.currentMenu,
			this.playlist.songs[currentSongIndex + 1].Begins.totalSeconds
		)

		return this
	}

	/** Goes to the previous track. */
	gotoPrevTrack(member: GuildMember, channel: TextChannel) {
		const currentSongIndex = this.playlist.getSongIndexAt(this.elapsedSeconds)
		if (currentSongIndex <= 0) return this.stop()
		this.play(
			member, channel, this.currentMenu,
			this.playlist.songs[currentSongIndex - 1].Begins.totalSeconds
		)

		return this
	}

	/** Replays the current track. */
	replayTrack(member: GuildMember, channel: TextChannel) {
		const currentSongIndex = this.playlist.getSongIndexAt(this.elapsedSeconds)
		this.play(
			member, channel, this.currentMenu,
			this.playlist.songs[currentSongIndex].Begins.totalSeconds
		)

		return this
	}

	pause() {
		this.editPause()
		if (this.player && !this.player.paused) this.player.pause()

		return this
	}

	resume() {
		this.editPlay()
		if (this.player && this.player.paused) this.player.resume()

		return this
	}

	playpause() {
		if (this.player) {
			if (this.player.paused) this.resume()
			else this.pause()
		}

		return this
	}

	stop() {
		this.editStop()
		delete this.currentMenu

		if (this.voiceChannel) {
			this.voiceChannel.leave()
			delete this.voiceChannel
		}

		return this
	}

	registerDefaultTypes() {
		this.types.push(...defaultTypes)
		return this
	}

	registerTypes(...types: ArgType<any>[]) {
		this.types.push(...types)
		return this
	}

	registerCommands(...commandinfos: WMPCommandInfo[]) {
		this.commandManager = new WMPCommandManager(this.types, commandinfos)
		return this
	}

	execute(cmd: string, msg: Message) {
		if (msg.channel.type !== 'text') throw new Error('Channel is not a TextChannel!')

		const state = this.commandManager.parse(msg.member, msg.channel, this, cmd)
		if (state.error) sendError(msg.channel, state.error.info)
		else state.result.execute(state.result)
	}
}
